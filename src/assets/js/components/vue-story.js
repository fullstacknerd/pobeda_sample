import Vue from 'vue'
// import FsLightbox from 'fslightbox-vue'


Vue.component('vue-story', {
    // components: { FsLightbox },

    props: {

        context: {
            type: Object,
            required: true,
            default: {
                title: '',
            }
        },

        storiesTotal: {
            type: Number,
            reuqired: true,
            default: 0,
        },

        index: {
            type: Number,
            reuqired: true,
            default: 0,
        }
    },

    data: function () {
        return {
            toggler: false,
            slide: 1,
            id: null,
        }
    },

    computed: {
        showCovers () {
            return this.context.covers.length > 0
        },

        coversCount () {
            if (this.showCovers) {
                return `m-${this.context.covers.length}`
            }
        },
    },

    created: function() {
        this.id = this.context.id
    },

    template: '#vue-story-template',
})

