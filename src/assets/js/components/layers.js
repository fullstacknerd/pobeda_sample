import Vue from 'vue'
import gsap from 'gsap'
import ScrollTrigger from 'gsap/ScrollTrigger'

gsap.registerPlugin(ScrollTrigger)


Vue.component('layers', {

    data: function () {
        return {
            cHeight: 0,
        }
    },

    mounted: function() {},

    methods: {
        onStoriesLoaded: function() {
            const layers = this.$el.querySelectorAll('.layer')

            this.calculateHeights(layers)
            this.animateLayers(layers)
            this.$parent.makePageInteractive()
        },

        calculateHeights: function(elems) {
            elems.forEach((el) => {
                this.cHeight += el.offsetHeight
            })
        },

        animateLayers: function (elems) {
            
            gsap.to(elems, {
                yPercent: -100, 
                ease: 'none',
                stagger: .5,
                lazy: true,
                scrollTrigger: {
                    trigger: '#container',
                    start: 'top top',
                    end: `${this.cHeight}px`,
                    scrub: true,
                    pin: true,
                    markers: true,
                    pinSpacing: true
                },
            })
            
            gsap.set(elems, {
                zIndex: (i, el, elems) => elems.length - i
            })
        },
    },
})
