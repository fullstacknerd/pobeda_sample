/* eslint-disable semi */
/* eslint-disable func-names */
/* eslint-disable object-shorthand */


import Vue from 'vue'
import store from './store/store'
import './components/_components'


window.AppRoot = new Vue({

    el: '#app',

    store,

    data: {
        isMobile: window.matchMedia('(max-width: 768px)').matches,
        isMainPage: document.querySelector('body').classList.contains('page_home'),
        showLoader: true,
        animateIntro: false,
    },

    watch: {},

    created() {
        if (this.isMainPage) {
            this.makeBodyFixed()
        }
    },

    mounted() {
        if (this.isMainPage) {
            this.$store.dispatch('UPDATE_STORIES')
        } else {
            this.makePageInteractive()
        }
    },

    beforeDestroy () {
        if (this.isMainPage) {
            window.scrollTo(0, 0)
        }
    },

    methods: {
        setBodyHeight: function() {
            const body = document.querySelector('body')
            body.style.height = `${body.offsetHeight}px`
        },

        makeBodyFixed: function() {
            const body = document.querySelector('body')
            
            body.style.overflowY = 'scroll'
            body.style.position = 'fixed'
        },

        removeBodyFixed: function() {
            const body = document.querySelector('body')
            
            body.style.overflowY = 'initial'
            body.style.position = 'relative'
        },

        makePageInteractive: function() {
            this.showLoader = false
            this.animateIntro = true
            this.removeBodyFixed()
        },
    }
})
